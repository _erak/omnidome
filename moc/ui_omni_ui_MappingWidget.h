/********************************************************************************
** Form generated from reading UI file 'omni_ui_MappingWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OMNI_UI_MAPPINGWIDGET_H
#define UI_OMNI_UI_MAPPINGWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>
#include <slim/GradientSlider.h>
#include <slim/RangedFloat.h>

namespace omni {
namespace ui {

class Ui_MappingWidget
{
public:
    slim::GradientSlider *titleBar;
    slim::RangedFloat *widget_2;
    slim::RangedFloat *widget_3;

    void setupUi(QWidget *omni__ui__MappingWidget)
    {
        if (omni__ui__MappingWidget->objectName().isEmpty())
            omni__ui__MappingWidget->setObjectName(QStringLiteral("omni__ui__MappingWidget"));
        omni__ui__MappingWidget->resize(400, 334);
        titleBar = new slim::GradientSlider(omni__ui__MappingWidget);
        titleBar->setObjectName(QStringLiteral("titleBar"));
        titleBar->setGeometry(QRect(50, 10, 120, 80));
        widget_2 = new slim::RangedFloat(omni__ui__MappingWidget);
        widget_2->setObjectName(QStringLiteral("widget_2"));
        widget_2->setGeometry(QRect(50, 150, 120, 80));
        widget_3 = new slim::RangedFloat(omni__ui__MappingWidget);
        widget_3->setObjectName(QStringLiteral("widget_3"));
        widget_3->setGeometry(QRect(240, 150, 120, 80));

        retranslateUi(omni__ui__MappingWidget);

        QMetaObject::connectSlotsByName(omni__ui__MappingWidget);
    } // setupUi

    void retranslateUi(QWidget *omni__ui__MappingWidget)
    {
        omni__ui__MappingWidget->setWindowTitle(QApplication::translate("omni::ui::MappingWidget", "Form", 0));
    } // retranslateUi

};

} // namespace ui
} // namespace omni

namespace omni {
namespace ui {
namespace Ui {
    class MappingWidget: public Ui_MappingWidget {};
} // namespace Ui
} // namespace ui
} // namespace omni

#endif // UI_OMNI_UI_MAPPINGWIDGET_H
