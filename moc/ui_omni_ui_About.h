/********************************************************************************
** Form generated from reading UI file 'omni_ui_About.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OMNI_UI_ABOUT_H
#define UI_OMNI_UI_ABOUT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include "AboutGL.h"

namespace omni {
namespace ui {

class Ui_About
{
public:
    QVBoxLayout *verticalLayout;
    omni::ui::AboutGL *widget;
    QPushButton *pushButton;

    void setupUi(QDialog *omni__ui__About)
    {
        if (omni__ui__About->objectName().isEmpty())
            omni__ui__About->setObjectName(QStringLiteral("omni__ui__About"));
        omni__ui__About->setWindowModality(Qt::ApplicationModal);
        omni__ui__About->resize(400, 300);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(omni__ui__About->sizePolicy().hasHeightForWidth());
        omni__ui__About->setSizePolicy(sizePolicy);
        omni__ui__About->setContextMenuPolicy(Qt::NoContextMenu);
        verticalLayout = new QVBoxLayout(omni__ui__About);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        widget = new omni::ui::AboutGL(omni__ui__About);
        widget->setObjectName(QStringLiteral("widget"));

        verticalLayout->addWidget(widget);

        pushButton = new QPushButton(omni__ui__About);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout->addWidget(pushButton);


        retranslateUi(omni__ui__About);
        QObject::connect(pushButton, SIGNAL(clicked()), omni__ui__About, SLOT(accept()));

        QMetaObject::connectSlotsByName(omni__ui__About);
    } // setupUi

    void retranslateUi(QDialog *omni__ui__About)
    {
        omni__ui__About->setWindowTitle(QApplication::translate("omni::ui::About", "About Omnidome Mapper", 0));
        pushButton->setText(QApplication::translate("omni::ui::About", "Close", 0));
    } // retranslateUi

};

} // namespace ui
} // namespace omni

namespace omni {
namespace ui {
namespace Ui {
    class About: public Ui_About {};
} // namespace Ui
} // namespace ui
} // namespace omni

#endif // UI_OMNI_UI_ABOUT_H
