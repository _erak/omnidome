/********************************************************************************
** Form generated from reading UI file 'omni_ui_Mapping.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OMNI_UI_MAPPING_H
#define UI_OMNI_UI_MAPPING_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

namespace omni {
namespace ui {

class Ui_Mapping
{
public:
    QVBoxLayout *verticalLayout;
    QComboBox *boxMappingSelect;

    void setupUi(QWidget *omni__ui__Mapping)
    {
        if (omni__ui__Mapping->objectName().isEmpty())
            omni__ui__Mapping->setObjectName(QStringLiteral("omni__ui__Mapping"));
        omni__ui__Mapping->resize(374, 200);
        verticalLayout = new QVBoxLayout(omni__ui__Mapping);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        boxMappingSelect = new QComboBox(omni__ui__Mapping);
        boxMappingSelect->setObjectName(QStringLiteral("boxMappingSelect"));

        verticalLayout->addWidget(boxMappingSelect);


        retranslateUi(omni__ui__Mapping);

        QMetaObject::connectSlotsByName(omni__ui__Mapping);
    } // setupUi

    void retranslateUi(QWidget *omni__ui__Mapping)
    {
        omni__ui__Mapping->setWindowTitle(QApplication::translate("omni::ui::Mapping", "Form", 0));
    } // retranslateUi

};

} // namespace ui
} // namespace omni

namespace omni {
namespace ui {
namespace Ui {
    class Mapping: public Ui_Mapping {};
} // namespace Ui
} // namespace ui
} // namespace omni

#endif // UI_OMNI_UI_MAPPING_H
