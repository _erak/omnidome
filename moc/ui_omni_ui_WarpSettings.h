/********************************************************************************
** Form generated from reading UI file 'omni_ui_WarpSettings.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OMNI_UI_WARPSETTINGS_H
#define UI_OMNI_UI_WARPSETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form
{
public:
    QVBoxLayout *verticalLayout;
    QPushButton *btnResizeWarp;
    QFrame *warpSizeFrame;
    QFormLayout *formLayout_3;
    QLabel *lbHorizontal;
    QSpinBox *boxHorizontal;
    QLabel *lbVertical;
    QSpinBox *boxVertical;
    QPushButton *btnReset;
    QPushButton *btnResetBezier;
    QPushButton *btnShowBezier;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QStringLiteral("Form"));
        Form->resize(430, 407);
        verticalLayout = new QVBoxLayout(Form);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        btnResizeWarp = new QPushButton(Form);
        btnResizeWarp->setObjectName(QStringLiteral("btnResizeWarp"));
        btnResizeWarp->setCheckable(true);
        btnResizeWarp->setFlat(false);

        verticalLayout->addWidget(btnResizeWarp);

        warpSizeFrame = new QFrame(Form);
        warpSizeFrame->setObjectName(QStringLiteral("warpSizeFrame"));
        formLayout_3 = new QFormLayout(warpSizeFrame);
        formLayout_3->setObjectName(QStringLiteral("formLayout_3"));
        lbHorizontal = new QLabel(warpSizeFrame);
        lbHorizontal->setObjectName(QStringLiteral("lbHorizontal"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, lbHorizontal);

        boxHorizontal = new QSpinBox(warpSizeFrame);
        boxHorizontal->setObjectName(QStringLiteral("boxHorizontal"));
        boxHorizontal->setMinimum(2);
        boxHorizontal->setMaximum(12);
        boxHorizontal->setValue(6);

        formLayout_3->setWidget(0, QFormLayout::FieldRole, boxHorizontal);

        lbVertical = new QLabel(warpSizeFrame);
        lbVertical->setObjectName(QStringLiteral("lbVertical"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, lbVertical);

        boxVertical = new QSpinBox(warpSizeFrame);
        boxVertical->setObjectName(QStringLiteral("boxVertical"));
        boxVertical->setMinimum(2);
        boxVertical->setMaximum(12);
        boxVertical->setValue(6);

        formLayout_3->setWidget(1, QFormLayout::FieldRole, boxVertical);


        verticalLayout->addWidget(warpSizeFrame);

        btnReset = new QPushButton(Form);
        btnReset->setObjectName(QStringLiteral("btnReset"));

        verticalLayout->addWidget(btnReset);

        btnResetBezier = new QPushButton(Form);
        btnResetBezier->setObjectName(QStringLiteral("btnResetBezier"));
        btnResetBezier->setFlat(false);

        verticalLayout->addWidget(btnResetBezier);

        btnShowBezier = new QPushButton(Form);
        btnShowBezier->setObjectName(QStringLiteral("btnShowBezier"));
        btnShowBezier->setCheckable(true);

        verticalLayout->addWidget(btnShowBezier);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(Form);

        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QWidget *Form)
    {
        Form->setWindowTitle(QApplication::translate("Form", "Form", 0));
        btnResizeWarp->setText(QApplication::translate("Form", "Resize", 0));
        lbHorizontal->setText(QApplication::translate("Form", "Horizontal", 0));
        lbVertical->setText(QApplication::translate("Form", "Vertical", 0));
        btnReset->setText(QApplication::translate("Form", "Reset", 0));
        btnResetBezier->setText(QApplication::translate("Form", "Reset bezier", 0));
        btnShowBezier->setText(QApplication::translate("Form", "Show bezier", 0));
    } // retranslateUi

};

namespace Ui {
    class Form: public Ui_Form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OMNI_UI_WARPSETTINGS_H
