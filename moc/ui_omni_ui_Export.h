/********************************************************************************
** Form generated from reading UI file 'omni_ui_Export.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OMNI_UI_EXPORT_H
#define UI_OMNI_UI_EXPORT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

namespace omni {
namespace ui {

class Ui_Export
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *settingsFrame_2;
    QHBoxLayout *horizontalLayout_10;
    QLineEdit *editExportFilename;
    QPushButton *btnClear;
    QPushButton *btnExport;
    QGroupBox *grpSettings;
    QFormLayout *formLayout;
    QLabel *separateImagesLabel;
    QFrame *frame;
    QVBoxLayout *verticalLayout_10;
    QRadioButton *rbSepNone;
    QRadioButton *rbSepScreens;
    QRadioButton *rbSepProj;
    QLabel *imageTypeLabel;
    QFrame *imageTypeFrame;
    QVBoxLayout *verticalLayout_9;
    QRadioButton *rbUVWMap;
    QRadioButton *rbTextureCoordinates;
    QRadioButton *rbMappedInput;

    void setupUi(QWidget *omni__ui__Export)
    {
        if (omni__ui__Export->objectName().isEmpty())
            omni__ui__Export->setObjectName(QStringLiteral("omni__ui__Export"));
        omni__ui__Export->resize(1289, 863);
        verticalLayout = new QVBoxLayout(omni__ui__Export);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        settingsFrame_2 = new QFrame(omni__ui__Export);
        settingsFrame_2->setObjectName(QStringLiteral("settingsFrame_2"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(settingsFrame_2->sizePolicy().hasHeightForWidth());
        settingsFrame_2->setSizePolicy(sizePolicy);
        settingsFrame_2->setMaximumSize(QSize(16777215, 48));
        settingsFrame_2->setFrameShape(QFrame::StyledPanel);
        settingsFrame_2->setFrameShadow(QFrame::Sunken);
        horizontalLayout_10 = new QHBoxLayout(settingsFrame_2);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(4, 4, 4, 4);
        editExportFilename = new QLineEdit(settingsFrame_2);
        editExportFilename->setObjectName(QStringLiteral("editExportFilename"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(editExportFilename->sizePolicy().hasHeightForWidth());
        editExportFilename->setSizePolicy(sizePolicy1);
        editExportFilename->setReadOnly(true);

        horizontalLayout_10->addWidget(editExportFilename);

        btnClear = new QPushButton(settingsFrame_2);
        btnClear->setObjectName(QStringLiteral("btnClear"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(btnClear->sizePolicy().hasHeightForWidth());
        btnClear->setSizePolicy(sizePolicy2);

        horizontalLayout_10->addWidget(btnClear);

        btnExport = new QPushButton(settingsFrame_2);
        btnExport->setObjectName(QStringLiteral("btnExport"));
        sizePolicy2.setHeightForWidth(btnExport->sizePolicy().hasHeightForWidth());
        btnExport->setSizePolicy(sizePolicy2);
        btnExport->setStyleSheet(QStringLiteral(""));

        horizontalLayout_10->addWidget(btnExport);


        verticalLayout->addWidget(settingsFrame_2);

        grpSettings = new QGroupBox(omni__ui__Export);
        grpSettings->setObjectName(QStringLiteral("grpSettings"));
        formLayout = new QFormLayout(grpSettings);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        separateImagesLabel = new QLabel(grpSettings);
        separateImagesLabel->setObjectName(QStringLiteral("separateImagesLabel"));

        formLayout->setWidget(0, QFormLayout::LabelRole, separateImagesLabel);

        frame = new QFrame(grpSettings);
        frame->setObjectName(QStringLiteral("frame"));
        verticalLayout_10 = new QVBoxLayout(frame);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        rbSepNone = new QRadioButton(frame);
        rbSepNone->setObjectName(QStringLiteral("rbSepNone"));
        rbSepNone->setChecked(true);

        verticalLayout_10->addWidget(rbSepNone);

        rbSepScreens = new QRadioButton(frame);
        rbSepScreens->setObjectName(QStringLiteral("rbSepScreens"));
        rbSepScreens->setChecked(false);

        verticalLayout_10->addWidget(rbSepScreens);

        rbSepProj = new QRadioButton(frame);
        rbSepProj->setObjectName(QStringLiteral("rbSepProj"));

        verticalLayout_10->addWidget(rbSepProj);


        formLayout->setWidget(0, QFormLayout::FieldRole, frame);

        imageTypeLabel = new QLabel(grpSettings);
        imageTypeLabel->setObjectName(QStringLiteral("imageTypeLabel"));

        formLayout->setWidget(1, QFormLayout::LabelRole, imageTypeLabel);

        imageTypeFrame = new QFrame(grpSettings);
        imageTypeFrame->setObjectName(QStringLiteral("imageTypeFrame"));
        verticalLayout_9 = new QVBoxLayout(imageTypeFrame);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        rbUVWMap = new QRadioButton(imageTypeFrame);
        rbUVWMap->setObjectName(QStringLiteral("rbUVWMap"));
        rbUVWMap->setChecked(true);

        verticalLayout_9->addWidget(rbUVWMap);

        rbTextureCoordinates = new QRadioButton(imageTypeFrame);
        rbTextureCoordinates->setObjectName(QStringLiteral("rbTextureCoordinates"));

        verticalLayout_9->addWidget(rbTextureCoordinates);

        rbMappedInput = new QRadioButton(imageTypeFrame);
        rbMappedInput->setObjectName(QStringLiteral("rbMappedInput"));

        verticalLayout_9->addWidget(rbMappedInput);


        formLayout->setWidget(1, QFormLayout::FieldRole, imageTypeFrame);


        verticalLayout->addWidget(grpSettings);


        retranslateUi(omni__ui__Export);

        QMetaObject::connectSlotsByName(omni__ui__Export);
    } // setupUi

    void retranslateUi(QWidget *omni__ui__Export)
    {
        omni__ui__Export->setWindowTitle(QApplication::translate("omni::ui::Export", "Form", 0));
        btnClear->setText(QApplication::translate("omni::ui::Export", "Clear", 0));
        btnExport->setText(QApplication::translate("omni::ui::Export", "EXPORT", 0));
        grpSettings->setTitle(QApplication::translate("omni::ui::Export", "Settings", 0));
        separateImagesLabel->setText(QApplication::translate("omni::ui::Export", "Separate Output", 0));
        rbSepNone->setText(QApplication::translate("omni::ui::Export", "No Separation (merge all together)", 0));
        rbSepScreens->setText(QApplication::translate("omni::ui::Export", "Separate Screens", 0));
        rbSepProj->setText(QApplication::translate("omni::ui::Export", "Separate Projectors", 0));
        imageTypeLabel->setText(QApplication::translate("omni::ui::Export", "Image Type", 0));
        rbUVWMap->setText(QApplication::translate("omni::ui::Export", "UVW Map", 0));
        rbTextureCoordinates->setText(QApplication::translate("omni::ui::Export", "Texture Coordinates", 0));
        rbMappedInput->setText(QApplication::translate("omni::ui::Export", "Mapped Input", 0));
    } // retranslateUi

};

} // namespace ui
} // namespace omni

namespace omni {
namespace ui {
namespace Ui {
    class Export: public Ui_Export {};
} // namespace Ui
} // namespace ui
} // namespace omni

#endif // UI_OMNI_UI_EXPORT_H
